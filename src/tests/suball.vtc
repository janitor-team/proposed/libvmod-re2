# looks like -*- vcl -*-

varnishtest "suball() method and function"

server s1 {
	rxreq
	txresp
} -start

varnish v1 -vcl+backend {
	import ${vmod_re2};

	sub vcl_init {
	       new piglatin = re2.regex("(qu|[b-df-hj-np-tv-z]*)([a-z]+)");
	       new word = re2.regex("\w+");
	       new start = re2.regex("^");
	       new end = re2.regex("$");
	       new b = re2.regex("b");
	       new bplus = re2.regex("b+");
	       new bstar = re2.regex("b*");
	}

	sub vcl_deliver {
		# Tests from re2 testing/re2_test.cc
		set resp.http.pangram =
		piglatin.suball("the quick brown fox jumps over the lazy dogs.",
		                "\2\1ay");
		set resp.http.nospam = word.suball("abcd.efghi@google.com",
				                   "\0-NOSPAM");
		set resp.http.startfoo = start.suball("foo", "(START)");
		set resp.http.start = start.suball("", "(START)");
		set resp.http.end = end.suball("", "(END)");
		set resp.http.ab = b.suball("ababababab", "bb");
		set resp.http.bb = b.suball("bbbbbb", "bb");
		set resp.http.bplus = bplus.suball("bbbbbb", "bb");
		set resp.http.bstar1 = bstar.suball("bbbbbb", "bb");
		set resp.http.bstar2 = bstar.suball("aaaaa", "bb");

		set resp.http.pangramf
		     = re2.suball("(qu|[b-df-hj-np-tv-z]*)([a-z]+)",
		                "the quick brown fox jumps over the lazy dogs.",
		                "\2\1ay");
		set resp.http.nospamf = re2.suball("\w+",
				                   "abcd.efghi@google.com",
				                   "\0-NOSPAM");
		set resp.http.startfoof = re2.suball("^", "foo", "(START)");
		set resp.http.startf = re2.suball("^", "", "(START)");
		set resp.http.endf = re2.suball("$", "", "(END)");
		set resp.http.abf = re2.suball("b", "ababababab", "bb");
		set resp.http.bbf = re2.suball("b", "bbbbbb", "bb");
		set resp.http.bplusf = re2.suball("b+", "bbbbbb", "bb");
		set resp.http.bstar1f = re2.suball("b*", "bbbbbb", "bb");
		set resp.http.bstar2f = re2.suball("b*", "aaaaa", "bb");

		# Match failure
		set resp.http.bfail = b.suball("acd", "x", "fallbackb");
		set resp.http.bfailf = re2.suball("b", "acd", "x", "fallbackf");

		# Undefined fallback
		if (req.http.Test == "fallbackundef") {
			if (req.http.Call == "method") {
				set resp.http.undeffallback
				    = b.suball("b", "x", req.http.undef);
			}
			elsif (req.http.Call == "function") {
				set resp.http.undeffallbackf
				    = re2.suball("b", "b", "x", req.http.undef);
			}
		}

		# Undefined pattern in the function
		if (req.http.Test == "patternundef") {
			set resp.http.undefpattern
			    = re2.suball(req.http.undef, "", "",
				"pattern undef");
		}

		# Undefined text
		if (req.http.Test == "textundef") {
			if (req.http.Call == "method") {
				set resp.http.undeftext
				    = b.suball(req.http.undef, "x",
					"text undef");
			}
			elsif (req.http.Call == "function") {
				set resp.http.undeftextf
				    = re2.suball("b", req.http.undef, "x",
					"text undef");
			}
		}

		# Undefined rewrite
		if (req.http.Test == "rewriteundef") {
			if (req.http.Call == "method") {
				set resp.http.undefrewrite
				    = b.suball("b", req.http.undef,
					"rewrite undef");
			}
			elsif (req.http.Call == "function") {
				set resp.http.undefrewritef
				    = re2.suball("b", "b", req.http.undef,
					"rewrite undef");
			}
		}

		# Default fallbacks
		set resp.http.fallback = b.suball("acd", "x");
		set resp.http.fallbackf = re2.suball("b", "acd", "x");
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.http.pangram == "ethay ickquay ownbray oxfay umpsjay overay ethay azylay ogsday."
	expect resp.http.nospam == "abcd-NOSPAM.efghi-NOSPAM@google-NOSPAM.com-NOSPAM"
	expect resp.http.startfoo == "(START)foo"
	expect resp.http.start == "(START)"
	expect resp.http.end == "(END)"
	expect resp.http.ab == "abbabbabbabbabb"
	expect resp.http.bb == "bbbbbbbbbbbb"
	expect resp.http.bplus == "bb"
	expect resp.http.bstar1 == "bb"
	expect resp.http.bstar2 == "bbabbabbabbabbabb"

	expect resp.http.pangramf == "ethay ickquay ownbray oxfay umpsjay overay ethay azylay ogsday."
	expect resp.http.nospamf == "abcd-NOSPAM.efghi-NOSPAM@google-NOSPAM.com-NOSPAM"
	expect resp.http.startfoof == "(START)foo"
	expect resp.http.startf == "(START)"
	expect resp.http.endf == "(END)"
	expect resp.http.abf == "abbabbabbabbabb"
	expect resp.http.bbf == "bbbbbbbbbbbb"
	expect resp.http.bplusf == "bb"
	expect resp.http.bstar1f == "bb"
	expect resp.http.bstar2f == "bbabbabbabbabbabb"

	expect resp.http.bfail == "fallbackb"
	expect resp.http.bfailf == "fallbackf"

	expect resp.http.fallback == "**SUBALL METHOD FAILED**"
	expect resp.http.fallbackf == "**SUBALL FUNCTION FAILED**"
} -run

client c1 {
	txreq -hdr "Test: fallbackundef" -hdr "Call: method"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undeffallback == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: fallbackundef" -hdr "Call: function"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undeffallbackf == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: patternundef"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undefpattern == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: textundef" -hdr "Call: method"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undeftext == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: textundef" -hdr "Call: function"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undeftextf == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: rewriteundef" -hdr "Call: method"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undefrewrite == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: rewriteundef" -hdr "Call: function"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.undefrewritef == <undef>
	expect_close
} -run

logexpect l1 -v v1 -d 1 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: fallbackundef$}
	expect * = ReqHeader {^Call: method$}
	expect * = VCL_Error {^vmod re2 failure: b\.suball\(\): fallback is undefined$}
	expect 0 = RespHeader {^undeffallback: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: fallbackundef$}
	expect * = ReqHeader {^Call: function$}
	expect * = VCL_Error {^vmod re2 failure: re2\.suball\(\): fallback is undefined$}
	expect 0 = RespHeader {^undeffallbackf: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: patternundef$}
	expect * = VCL_Error {^vmod re2 failure: re2\.suball\(pattern=<undefined>, fallback="pattern undef"\): pattern is undefined$}
	expect 0 = RespHeader {^undefpattern: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: textundef$}
	expect * = ReqHeader {^Call: method$}
	expect * = VCL_Error {^vmod re2 failure: b\.suball\(text=<undefined>, fallback="[^"]+"\): text is undefined$}
	# "
	expect 0 = RespHeader {^undeftext: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: textundef$}
	expect * = ReqHeader {^Call: function$}
	expect * = VCL_Error {^vmod re2 failure: re2\.suball\(pattern="b", text=<undefined>, fallback="[^"]+"\): text is undefined$}
	# "
	expect 0 = RespHeader {^undeftextf: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: rewriteundef$}
	expect * = ReqHeader {^Call: method$}
	expect * = VCL_Error {^vmod re2 failure: b\.suball\(text="b", rewrite=<undefined>, fallback="[^"]+"\): rewrite is undefined$}
	# "
	expect 0 = RespHeader {^undefrewrite: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: rewriteundef$}
	expect * = ReqHeader {^Call: function$}
	expect * = VCL_Error {^vmod re2 failure: re2\.suball\(pattern="b", text="b", rewrite=<undefined>, fallback="[^"]+"\): rewrite is undefined$}
	# "
	expect 0 = RespHeader {^undefrewritef: $}
	expect 0 = VCL_return fail
	expect * = End
} -run
