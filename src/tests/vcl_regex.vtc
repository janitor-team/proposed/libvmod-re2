# looks like -*- vcl -*-

varnishtest "backrefs not affected by standard VCL regex code"

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
	    	new barbaz = re2.regex("(bar)baz");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo = "barbaz";
		if (!barbaz.match(resp.http.foo)) {
		        set resp.status = 999;
		}

		if (resp.http.foo ~ "bar(baz)") {
			set resp.http.tilde0 = barbaz.backref(0, "tilde0");
			set resp.http.tilde1 = barbaz.backref(1, "tilde1");
		} else {
			set resp.status = 999;
		}

		if (resp.http.foo !~ "bar(quux)") {
			set resp.http.neg0 = barbaz.backref(0, "neg0");
			set resp.http.neg1 = barbaz.backref(1, "neg1");
		} else {
			set resp.status = 999;
		}

		set resp.http.regsub
		    = regsub(resp.http.foo, "bar(baz)", "\1");
		set resp.http.regsub0 = barbaz.backref(0, "regsub0");
		set resp.http.regsub1 = barbaz.backref(1, "regsub1");

		set resp.http.regsuball
		    = regsuball(resp.http.foo, "(.)", "x");
		set resp.http.regsuball0 = barbaz.backref(0, "regsuball0");
		set resp.http.regsuball1 = barbaz.backref(1, "regsuball1");
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.tilde0 == "barbaz"
	expect resp.http.tilde1 == "bar"
	expect resp.http.neg0 == "barbaz"
	expect resp.http.neg1 == "bar"
	expect resp.http.regsub == "baz"
	expect resp.http.regsub0 == "barbaz"
	expect resp.http.regsub1 == "bar"
	expect resp.http.regsuball == "xxxxxx"
	expect resp.http.regsuball0 == "barbaz"
	expect resp.http.regsuball1 == "bar"
} -run

# match() function

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo = "barbaz";
		if (!re2.match("(bar)baz", resp.http.foo)) {
		        set resp.status = 999;
		}

		if (resp.http.foo ~ "bar(baz)") {
			set resp.http.tilde0 = re2.backref(0, "tilde0");
			set resp.http.tilde1 = re2.backref(1, "tilde1");
		} else {
			set resp.status = 999;
		}

		if (resp.http.foo !~ "bar(quux)") {
			set resp.http.neg0 = re2.backref(0, "neg0");
			set resp.http.neg1 = re2.backref(1, "neg1");
		} else {
			set resp.status = 999;
		}

		set resp.http.regsub
		    = regsub(resp.http.foo, "bar(baz)", "\1");
		set resp.http.regsub0 = re2.backref(0, "regsub0");
		set resp.http.regsub1 = re2.backref(1, "regsub1");

		set resp.http.regsuball
		    = regsuball(resp.http.foo, "(.)", "x");
		set resp.http.regsuball0 = re2.backref(0, "regsuball0");
		set resp.http.regsuball1 = re2.backref(1, "regsuball1");
	}
}

client c1 -run

# sub() method

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new barbaz = re2.regex("(bar)baz");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo = "barbaz";
		if (!barbaz.match(resp.http.foo)) {
		        set resp.status = 999;
		}

		if (resp.http.foo ~ "bar(baz)") {
			set resp.http.tilde =
				barbaz.sub(resp.http.foo, "\0\1", "tildesub");
		} else {
			set resp.status = 999;
		}

		if (resp.http.foo !~ "bar(quux)") {
			set resp.http.neg =
				barbaz.sub(resp.http.foo, "\0\1", "negsub");
		} else {
			set resp.status = 999;
		}

		set resp.http.regsub
		    = regsub(resp.http.foo, "bar(baz)", "\1");
		set resp.http.regsubsub = barbaz.sub(resp.http.foo, "\0\1",
                                                     "regsubsub");

		set resp.http.regsuball
		    = regsuball(resp.http.foo, "(.)", "x");
		set resp.http.regsuballsub = barbaz.sub(resp.http.foo, "\0\1",
                                                        "regsuballsub");
	}
}

client c2 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.tilde == "barbazbar"
	expect resp.http.neg == "barbazbar"
	expect resp.http.regsub == "baz"
	expect resp.http.regsubsub == "barbazbar"
	expect resp.http.regsuball == "xxxxxx"
	expect resp.http.regsuballsub == "barbazbar"
} -run

# suball() method

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new barbaz = re2.regex("(bar)baz");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo = "barbaz";
		if (!barbaz.match(resp.http.foo)) {
		        set resp.status = 999;
		}

		if (resp.http.foo ~ "bar(baz)") {
			set resp.http.tilde =
				barbaz.suball(resp.http.foo, "\0\1",
			                      "tildesub");
		} else {
			set resp.status = 999;
		}

		if (resp.http.foo !~ "bar(quux)") {
			set resp.http.neg =
				barbaz.suball(resp.http.foo, "\0\1", "negsub");
		} else {
			set resp.status = 999;
		}

		set resp.http.regsub
		    = regsub(resp.http.foo, "bar(baz)", "\1");
		set resp.http.regsubsub = barbaz.suball(resp.http.foo, "\0\1",
                                                        "regsubsub");

		set resp.http.regsuball
		    = regsuball(resp.http.foo, "(.)", "x");
		set resp.http.regsuballsub = barbaz.suball(resp.http.foo,
		                                           "\0\1",
                                                           "regsuballsub");
	}
}

client c2 -run

# extract() method

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new barbaz = re2.regex("(bar)baz");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo = "barbaz";
		if (!barbaz.match(resp.http.foo)) {
		        set resp.status = 999;
		}

		if (resp.http.foo ~ "bar(baz)") {
			set resp.http.tilde =
				barbaz.extract(resp.http.foo, "\0\1",
			                       "tildesub");
		} else {
			set resp.status = 999;
		}

		if (resp.http.foo !~ "bar(quux)") {
			set resp.http.neg =
				barbaz.extract(resp.http.foo, "\0\1", "negsub");
		} else {
			set resp.status = 999;
		}

		set resp.http.regsub
		    = regsub(resp.http.foo, "bar(baz)", "\1");
		set resp.http.regsubsub = barbaz.extract(resp.http.foo, "\0\1",
                                                         "regsubx");

		set resp.http.regsuball
		    = regsuball(resp.http.foo, "(.)", "x");
		set resp.http.regsuballsub = barbaz.extract(resp.http.foo,
		                                            "\0\1",
                                                            "regsuballx");
	}
}

client c2 -run
