# looks like -*- vcl -*-

varnishtest "set .which(), .string(), .backend() and .saved() methods"

server s1 {
	rxreq
	txresp
} -start

varnish v1 -vcl+backend {
	import ${vmod_re2};

	sub vcl_init {
		new s = re2.set();
		s.add("foo", "baz");
		s.add("bar", "quux");
	}

	sub vcl_deliver {
		set resp.http.s-str-1 = s.string(1);
		set resp.http.s-str-2 = s.string(2);
		set resp.http.s-str-saved-1 = s.saved(STR, 1);
		set resp.http.s-str-saved-2 = s.saved(STR, 2);
		if (req.http.Test == "b4match") {
			if (req.http.Call == "string") {
				set resp.http.s-before-match = s.string();
			}
			if (req.http.Call == "saved") {
				set resp.http.s-saved-before-match =
				    s.saved(STR);
			}
			if (req.http.Call == "which") {
				set resp.http.s-which-b4-match = s.which();
			}
		}
		set resp.http.s-foo-match = s.match("foo");
		set resp.http.s-foo-n = s.nmatches();
		set resp.http.s-foo-which = s.which();
		set resp.http.s-foo-str = s.string();
		set resp.http.s-foo-saved = s.saved(STR);
		set resp.http.s-bar-match = s.match("bar");
		set resp.http.s-bar-n = s.nmatches();
		set resp.http.s-bar-which = s.which();
		set resp.http.s-bar-str = s.string();
		set resp.http.s-bar-saved = s.saved(STR);
		set resp.http.s-bar-str-0 = s.string(0);
		set resp.http.s-bar-str-1 = s.string(-1);
		set resp.http.s-bar-saved-0 = s.saved(STR, 0);
		set resp.http.s-bar-saved-1 = s.saved(STR, -1);
		set resp.http.s-fail-match = s.match("fail");
		set resp.http.s-fail-n = s.nmatches();
		if (req.http.Test == "failmatch") {
			if (req.http.Call == "which") {
				set resp.http.s-fail-which = s.which();
			}
			if (req.http.Call == "string") {
				set resp.http.s-fail-str = s.string();
			}
			if (req.http.Call == "saved") {
				set resp.http.s-fail-saved = s.saved(STR);
			}
		}
		set resp.http.s-many-match = s.match("foobar");
		set resp.http.s-many-n = s.nmatches();
		set resp.http.s-many-first = s.string(select=FIRST);
		set resp.http.s-many-last = s.string(select=LAST);
		set resp.http.s-which-first = s.which(select=FIRST);
		set resp.http.s-which-last = s.which(select=LAST);
		set resp.http.s-many-saved-first = s.saved(STR, select=FIRST);
		set resp.http.s-many-saved-last = s.saved(STR, select=FIRST);
		if (req.http.Test == "manymatch") {
			if (req.http.Call == "which") {
				set resp.http.s-which-many = s.which();
			}
			if (req.http.Call == "string") {
				set resp.http.s-many-str = s.string();
			}
			if (req.http.Call == "saved") {
				set resp.http.s-many-saved = s.saved(STR);
			}
		}
		if (req.http.Test == "range") {
			set resp.http.s-outofrange = s.string(3);
		}
	}

} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.s-str-1 == "baz"
	expect resp.http.s-str-2 == "quux"
	expect resp.http.s-str-saved-1 == "true"
	expect resp.http.s-str-saved-2 == "true"
	expect resp.http.s-foo-match == "true"
	expect resp.http.s-foo-n == 1
	expect resp.http.s-foo-which == 1
	expect resp.http.s-foo-str == "baz"
	expect resp.http.s-foo-saved == "true"
	expect resp.http.s-bar-match == "true"
	expect resp.http.s-bar-n == 1
	expect resp.http.s-bar-which == 2
	expect resp.http.s-bar-str == "quux"
	expect resp.http.s-bar-saved == "true"
	expect resp.http.s-bar-str-0 == resp.http.s-bar-str
	expect resp.http.s-bar-str-1 == resp.http.s-bar-str
	expect resp.http.s-bar-saved-0 == "true"
	expect resp.http.s-bar-saved-1 == "true"
	expect resp.http.s-fail-match == "false"
	expect resp.http.s-fail-n == 0
	expect resp.http.s-many-match == "true"
	expect resp.http.s-many-n == 2
	expect resp.http.s-many-first == "baz"
	expect resp.http.s-many-last == "quux"
	expect resp.http.s-which-first == 1
	expect resp.http.s-which-last == 2
	expect resp.http.s-many-saved-first == "true"
	expect resp.http.s-many-saved-last == "true"
} -run

client c1 {
	txreq -hdr "Test: b4match" -hdr "Call: string"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: b4match" -hdr "Call: saved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-saved-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: b4match" -hdr "Call: which"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-which-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: failmatch" -hdr "Call: string"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-fail-str == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: failmatch" -hdr "Call: saved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-fail-saved == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: failmatch" -hdr "Call: which"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-fail-which == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: manymatch" -hdr "Call: which"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-which-many == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: manymatch" -hdr "Call: string"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-many-str == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: manymatch" -hdr "Call: saved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-many-saved == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: range"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-outofrange == <undef>
	expect_close
} -run

logexpect l1 -v v1 -d 1 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: b4match$}
	expect * = ReqHeader {^Call: string$}
	expect * = VCL_Error {^vmod re2 failure: s\.string\(\) called without prior match$}
	expect 0 = RespHeader {^s-before-match: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: b4match$}
	expect * = ReqHeader {^Call: saved$}
	expect * = VCL_Error {^vmod re2 failure: s\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^s-saved-before-match: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: b4match$}
	expect * = ReqHeader {^Call: which$}
	expect * = VCL_Error {^vmod re2 failure: s\.which\(\) called without prior match$}
	expect 0 = RespHeader {^s-which-b4-match: 0$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: failmatch$}
	expect * = ReqHeader {^Call: string$}
	expect * = VCL_Error {^vmod re2 failure: s\.string\(0\): previous match was unsuccessful$}
	expect 0 = RespHeader {^s-fail-str: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: failmatch$}
	expect * = ReqHeader {^Call: saved$}
	expect * = VCL_Error {^vmod re2 failure: s\.saved\(0\): previous match was unsuccessful$}
	expect 0 = RespHeader {^s-fail-saved: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: failmatch$}
	expect * = ReqHeader {^Call: which$}
	expect * = VCL_Error {^vmod re2 failure: s\.which\(0\): previous match was unsuccessful$}
	expect 0 = RespHeader {^s-fail-which: 0$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: manymatch$}
	expect * = ReqHeader {^Call: which$}
	expect * = VCL_Error {^vmod re2 failure: s\.which\(0\): 2 successful matches$}
	expect 0 = RespHeader {^s-which-many: 0$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: manymatch$}
	expect * = ReqHeader {^Call: string$}
	expect * = VCL_Error {^vmod re2 failure: s\.string\(0\): 2 successful matches$}
	expect 0 = RespHeader {^s-many-str: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: manymatch$}
	expect * = ReqHeader {^Call: saved$}
	expect * = VCL_Error {^vmod re2 failure: s\.saved\(0\): 2 successful matches$}
	expect 0 = RespHeader {^s-many-saved: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: range$}
	expect * = VCL_Error {^vmod re2 failure: s\.string\(3\): set has 2 patterns$}
	expect 0 = RespHeader {^s-outofrange: $}
	expect 0 = VCL_return fail
	expect * = End
} -run

varnish v1 -vcl+backend {
	import ${vmod_re2};
	import std;

	backend b0 None;

	sub vcl_init {
		new n = re2.set();
		n.add("foo", backend = b0);
	}

	sub vcl_deliver {
		if (req.http.Test == "string") {
			set resp.http.n-str-ref =
			    n.string(std.integer(req.http.Ref));
		}
		set resp.http.n-str-saved-1 = n.saved(STR, 1);
		if (req.http.Test == "saved_str") {
			set resp.http.n-str-saved-ref =
			    n.saved(STR, std.integer(req.http.Ref));
		}
		if (req.http.Test == "string_noarg") {
			set resp.http.n-before-match = n.string();
		}
		if (req.http.Test == "saved_noref") {
			set resp.http.n-saved-before-match = n.saved(STR);
		}
		set resp.http.n-foo-match = n.match("foo");
		set resp.http.n-foo-n = n.nmatches();
		if (req.http.Test == "string_after_match") {
			set resp.http.n-foo-str = n.string();
		}
		set resp.http.n-foo-saved = n.saved(STR);
		if (req.http.Test == "backend_none") {
			set resp.http.n-foo-backend = n.backend();
		}
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.n-str-saved-1 == "false"
	expect resp.http.n-foo-match == "true"
	expect resp.http.n-foo-n == 1
	expect resp.http.n-foo-saved == "false"
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: string$}
	expect * = ReqHeader {^Ref: 1$}
	expect * = VCL_Error {^vmod re2 failure: n\.string\(1\): No strings were set for n$}
	expect 0 = RespHeader {^n-str-ref: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: string$}
	expect * = ReqHeader {^Ref: 2$}
	expect * = VCL_Error {^vmod re2 failure: n\.string\(2\): No strings were set for n$}
	expect 0 = RespHeader {^n-str-ref: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_str$}
	expect * = ReqHeader {^Ref: -1$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^n-str-saved-ref: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_str$}
	expect * = ReqHeader {^Ref: 0$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^n-str-saved-ref: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_str$}
	expect * = ReqHeader {^Ref: 2$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(2\): set has 1 patterns$}
	expect 0 = RespHeader {^n-str-saved-ref: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: string_noarg$}
	expect * = VCL_Error {^vmod re2 failure: n\.string\(0\): No strings were set for n$}
	expect 0 = RespHeader {^n-before-match: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_noref$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^n-saved-before-match: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: string_after_match$}
	expect * = VCL_Error {^vmod re2 failure: n\.string\(0\): No strings were set for n$}
	expect 0 = RespHeader {^n-foo-str: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: backend_none$}
	expect * = VCL_Error {^vmod re2 failure: n\.backend\(0\): No backends were set for n$}
	expect 0 = RespHeader {^n-foo-backend: $}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Test: string" -hdr "Ref: 1"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-str-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: string" -hdr "Ref: 2"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-str-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_str" -hdr "Ref: -1"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-str-saved-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_str" -hdr "Ref: 0"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-str-saved-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_str" -hdr "Ref: 2"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-str-saved-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: string_noarg"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_noref"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-saved-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: string_after_match"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-foo-string == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: backend_none"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-foo-backend == <undef>
	expect_close
} -run

logexpect l1 -wait

varnish v1 -vcl+backend {
	import ${vmod_re2};
	backend b1 { .host = "${bad_ip}"; }
	backend b2 { .host = "${bad_ip}"; }

	sub vcl_init {
		new b = re2.set();
		b.add("foo", backend=b1);
		b.add("bar", backend=b2);
	}

	sub vcl_deliver {
		set resp.http.b-backend-1 = b.backend(1);
		set resp.http.b-backend-2 = b.backend(2);
		set resp.http.b-be-saved-1 = b.saved(BE, 1);
		set resp.http.b-be-saved-2 = b.saved(BE, 2);
		if (req.http.Test == "b4match") {
			if (req.http.Call == "backend") {
				set resp.http.b-before-match = b.backend();
			}
			if (req.http.Call == "saved") {
				set resp.http.b-saved-before-match =
				    b.saved(BE);
			}
		}
		set resp.http.b-foo-match = b.match("foo");
		set resp.http.b-foo-n = b.nmatches();
		set resp.http.b-foo-backend = b.backend();
		set resp.http.b-foo-saved = b.saved(BE);
		set resp.http.b-bar-match = b.match("bar");
		set resp.http.b-bar-n = b.nmatches();
		set resp.http.b-bar-backend = b.backend();
		set resp.http.b-bar-saved = b.saved(BE);
		set resp.http.b-bar-backend-0 = b.backend(0);
		set resp.http.b-bar-backend-1 = b.backend(-1);
		set resp.http.b-bar-saved-0 = b.saved(BE, 0);
		set resp.http.b-bar-saved-1 = b.saved(BE, -1);
		set resp.http.b-fail-match = b.match("fail");
		set resp.http.b-fail-n = b.nmatches();
		if (req.http.Test == "failmatch") {
			if (req.http.Call == "backend") {
				set resp.http.b-fail-backend = b.backend();
			}
			if (req.http.Call == "saved") {
				set resp.http.b-fail-saved = b.saved(BE);
			}
		}
		set resp.http.b-many-match = b.match("foobar");
		set resp.http.b-many-n = b.nmatches();
		set resp.http.b-many-first = b.backend(select=FIRST);
		set resp.http.b-many-last = b.backend(select=LAST);
		if (req.http.Test == "manymatch") {
			if (req.http.Call == "backend") {
				set resp.http.b-many-backend = b.backend();
			}
			if (req.http.Call == "saved") {
				set resp.http.b-many-saved = b.saved(BE);
			}
		}
		set resp.http.b-many-saved-first = b.saved(BE, select=FIRST);
		set resp.http.b-many-saved-last = b.saved(BE, select=LAST);
		if (req.http.Test == "range") {
			set resp.http.b-outofrange = b.backend(3);
		}
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.b-backend-1 == "b1"
	expect resp.http.b-backend-2 == "b2"
	expect resp.http.b-be-saved-1 == "true"
	expect resp.http.b-be-saved-2 == "true"
	expect resp.http.b-foo-match == "true"
	expect resp.http.b-foo-n == 1
	expect resp.http.b-foo-backend == "b1"
	expect resp.http.b-foo-saved == "true"
	expect resp.http.b-bar-match == "true"
	expect resp.http.b-bar-n == 1
	expect resp.http.b-bar-backend == "b2"
	expect resp.http.b-bar-saved == "true"
	expect resp.http.b-bar-backend-0 == resp.http.b-bar-backend
	expect resp.http.b-bar-backend-1 == resp.http.b-bar-backend
	expect resp.http.b-bar-saved-0 == "true"
	expect resp.http.b-bar-saved-1 == "true"
	expect resp.http.b-fail-match == "false"
	expect resp.http.b-fail-n == 0
	expect resp.http.b-many-match == "true"
	expect resp.http.b-many-n == 2
	expect resp.http.b-many-first == "b1"
	expect resp.http.b-many-last == "b2"
	expect resp.http.b-many-saved-first == "true"
	expect resp.http.b-many-saved-last == "true"
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: b4match$}
	expect * = ReqHeader {^Call: backend$}
	expect * = VCL_Error {^vmod re2 failure: b\.backend\(\) called without prior match$}
	expect 0 = RespHeader {^b-before-match: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: b4match$}
	expect * = ReqHeader {^Call: saved$}
	expect * = VCL_Error {^vmod re2 failure: b\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^b-saved-before-match: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: failmatch$}
	expect * = ReqHeader {^Call: backend$}
	expect * = VCL_Error {^vmod re2 failure: b\.backend\(0\): previous match was unsuccessful$}
	expect 0 = RespHeader {^b-fail-backend: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: failmatch$}
	expect * = ReqHeader {^Call: saved$}
	expect * = VCL_Error {^vmod re2 failure: b\.saved\(0\): previous match was unsuccessful$}
	expect 0 = RespHeader {^b-fail-saved: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: manymatch$}
	expect * = ReqHeader {^Call: backend$}
	expect * = VCL_Error {^vmod re2 failure: b\.backend\(0\): 2 successful matches$}
	expect 0 = RespHeader {^b-many-backend: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: manymatch$}
	expect * = ReqHeader {^Call: saved$}
	expect * = VCL_Error {^vmod re2 failure: b\.saved\(0\): 2 successful matches$}
	expect 0 = RespHeader {^b-many-saved: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: range$}
	expect * = VCL_Error {^vmod re2 failure: b\.backend\(3\): set has 2 patterns$}
	expect 0 = RespHeader {^b-outofrange: $}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Test: b4match" -hdr "Call: backend"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: b4match" -hdr "Call: saved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-saved-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: failmatch" -hdr "Call: backend"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-fail-backend == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: failmatch" -hdr "Call: saved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-fail-saved == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: manymatch" -hdr "Call: backend"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-many-backend == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: manymatch" -hdr "Call: saved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-many-saved == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: range"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-outofrange == <undef>
	expect_close
} -run

logexpect l1 -wait

varnish v1 -vcl+backend {
	import ${vmod_re2};
	import std;

	sub vcl_init {
		new n = re2.set();
		n.add("foo");
	}

	sub vcl_deliver {
		if (req.http.Test == "backend_ref") {
			# XXX 1 2
			set resp.http.n-backend-ref =
			    n.backend(std.integer(req.http.Ref));
		}
		if (req.http.Test == "saved_ref") {
			# XXX -1 0 1 2
			set resp.http.n-backend-saved-ref =
			    n.saved(BE, std.integer(req.http.Ref));
		}
		if (req.http.Test == "backend_noarg") {
			set resp.http.n-before-match = n.backend();
		}
		if (req.http.Test == "saved_noref") {
			set resp.http.n-saved-before-match = n.saved(BE);
		}
		set resp.http.n-foo-match = n.match("foo");
		set resp.http.n-foo-n = n.nmatches();
		set resp.http.n-foo-saved = n.saved(BE);
		if (req.http.Test == "backend_none") {
			set resp.http.n-foo-backend = n.backend();
		}
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.n-foo-match == "true"
	expect resp.http.n-foo-n == 1
	expect resp.http.n-foo-saved == "false"
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: backend_ref$}
	expect * = ReqHeader {^Ref: 1$}
	expect * = VCL_Error {^vmod re2 failure: n\.backend\(1\): No backends were set for n$}
	expect 0 = RespHeader {^n-backend-ref: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: backend_ref$}
	expect * = ReqHeader {^Ref: 2$}
	expect * = VCL_Error {^vmod re2 failure: n\.backend\(2\): No backends were set for n$}
	expect 0 = RespHeader {^n-backend-ref: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_ref$}
	expect * = ReqHeader {^Ref: -1$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^n-backend-saved-ref: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_ref$}
	expect * = ReqHeader {^Ref: 0$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^n-backend-saved-ref: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_ref$}
	expect * = ReqHeader {^Ref: 2$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(2\): set has 1 patterns$}
	expect 0 = RespHeader {^n-backend-saved-ref: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: backend_noarg$}
	expect * = VCL_Error {^vmod re2 failure: n\.backend\(0\): No backends were set for n$}
	expect 0 = RespHeader {^n-before-match: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_noref$}
	expect * = VCL_Error {^vmod re2 failure: n\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^n-saved-before-match: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: backend_none$}
	expect * = VCL_Error {^vmod re2 failure: n\.backend\(0\): No backends were set for n$}
	expect 0 = RespHeader {^n-foo-backend: $}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Test: backend_ref" -hdr "Ref: 1"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-backend-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: backend_ref" -hdr "Ref: 2"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-backend-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_ref" -hdr "Ref: -1"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-backend-saved-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_ref" -hdr "Ref: 0"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-backend-saved-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_ref" -hdr "Ref: 2"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-backend-saved-ref == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: backend_noarg"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_noref"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-saved-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: backend_none"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.n-foo-backend == <undef>
	expect_close
} -run

logexpect l1 -wait

varnish v1 -vcl {
	import ${vmod_re2};
	backend be { .host = "${bad_ip}"; }

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
		s.add("bar", "baz");

		new b = re2.set();
		b.add("foo", backend=be);
		b.add("bar");
	}

	sub vcl_deliver {
		if (req.http.Test == "str_unsaved") {
			set resp.http.s-str-1 = s.string(1);
		}
		set resp.http.s-str-2 = s.string(2);
		set resp.http.s-saved-1 = s.saved(STR, 1);
		set resp.http.s-saved-2 = s.saved(STR, 2);
		set resp.http.s-match = s.match("foobar");
		if (req.http.Test == "str_select_unsaved") {
			set resp.http.s-first = s.string(select=FIRST);
		}
		set resp.http.s-last = s.string(select=LAST);
		set resp.http.s-saved-first = s.saved(STR, select=FIRST);
		set resp.http.s-saved-last = s.saved(STR, select=LAST);

		set resp.http.b-backend-1 = b.backend(1);
		if (req.http.Test == "be_unsaved") {
			set resp.http.b-backend-2 = b.backend(2);
		}
		set resp.http.b-saved-1 = b.saved(BE, 1);
		set resp.http.b-saved-2 = b.saved(BE, 2);
		set resp.http.b-match = b.match("foobar");
		set resp.http.b-first = b.backend(select=FIRST);
		if (req.http.Test == "be_select_unsaved") {
			set resp.http.b-last = b.backend(select=LAST);
		}
		set resp.http.b-saved-first = b.saved(BE, select=FIRST);
		set resp.http.b-saved-last = b.saved(BE, select=LAST);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.s-str-2 == "baz"
	expect resp.http.s-saved-1 == "false"
	expect resp.http.s-saved-2 == "true"
	expect resp.http.s-match == "true"
	expect resp.http.s-saved-first == resp.http.s-saved-1
	expect resp.http.s-saved-last == resp.http.s-saved-2
	expect resp.http.b-backend-1 == "be"
	expect resp.http.b-saved-1 == "true"
	expect resp.http.b-saved-2 == "false"
	expect resp.http.b-match == "true"
	expect resp.http.b-first == resp.http.b-backend-1
	expect resp.http.b-last == resp.http.b-backend-2
	expect resp.http.b-saved-first == resp.http.b-saved-1
	expect resp.http.b-saved-last == resp.http.b-saved-2
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: str_unsaved$}
	expect * = VCL_Error {^vmod re2 failure: s\.string\(1, UNIQUE\): String 1 was not added$}
	expect 0 = RespHeader {^s-str-1: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: str_select_unsaved$}
	expect * = VCL_Error {^vmod re2 failure: s\.string\(0, FIRST\): String 1 was not added$}
	expect 0 = RespHeader {^s-first: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: be_unsaved$}
	expect * = VCL_Error {^vmod re2 failure: b\.backend\(2, UNIQUE\): Backend 2 was not added$}
	expect 0 = RespHeader {^b-backend-2: $}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: be_select_unsaved$}
	expect * = VCL_Error {^vmod re2 failure: b\.backend\(0, LAST\): Backend 2 was not added$}
	expect 0 = RespHeader {^b-last: $}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Test: str_unsaved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-str-1 == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: str_select_unsaved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.s-first == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: be_unsaved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-backend-2 == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: be_select_unsaved"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.b-last == <undef>
	expect_close
} -run

logexpect l1 -wait

varnish v1 -vcl+backend {
	import ${vmod_re2};

	sub vcl_init {
		new i = re2.set();
		i.add("foo", integer=47);
		i.add("bar", integer=-11);
	}

	sub vcl_deliver {
		set resp.http.i-integer-1 = i.integer(1);
		set resp.http.i-integer-2 = i.integer(2);
		set resp.http.i-int-saved-1 = i.saved(INT, 1);
		set resp.http.i-int-saved-2 = i.saved(INT, 2);
		if (req.http.Test == "saved_b4_match") {
			set resp.http.i-saved-before-match = i.saved(INT);
		}
		set resp.http.i-foo-match = i.match("foo");
		set resp.http.i-foo-n = i.nmatches();
		set resp.http.i-foo-integer = i.integer();
		set resp.http.i-foo-saved = i.saved(INT);
		set resp.http.i-bar-match = i.match("bar");
		set resp.http.i-bar-n = i.nmatches();
		set resp.http.i-bar-integer = i.integer();
		set resp.http.i-bar-saved = i.saved(INT);
		set resp.http.i-bar-integer-0 = i.integer(0);
		set resp.http.i-bar-integer-1 = i.integer(-1);
		set resp.http.i-bar-saved-0 = i.saved(INT, 0);
		set resp.http.i-bar-saved-1 = i.saved(INT, -1);
		set resp.http.i-fail-match = i.match("fail");
		set resp.http.i-fail-n = i.nmatches();
		if (req.http.Test == "saved_fail") {
			set resp.http.i-fail-saved = i.saved(INT);
		}
		set resp.http.i-many-match = i.match("foobar");
		set resp.http.i-many-n = i.nmatches();
		set resp.http.i-many-first = i.integer(select=FIRST);
		set resp.http.i-many-last = i.integer(select=LAST);
		if (req.http.Test == "saved_unique") {
			set resp.http.i-many-saved = i.saved(INT);
		}
		set resp.http.i-many-saved-first = i.saved(INT, select=FIRST);
		set resp.http.i-many-saved-last = i.saved(INT, select=LAST);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.i-integer-1 == 47
	expect resp.http.i-integer-2 == -11
	expect resp.http.i-int-saved-1 == "true"
	expect resp.http.i-int-saved-2 == "true"
	expect resp.http.i-foo-match == "true"
	expect resp.http.i-foo-n == 1
	expect resp.http.i-foo-integer == 47
	expect resp.http.i-foo-saved == "true"
	expect resp.http.i-bar-match == "true"
	expect resp.http.i-bar-n == 1
	expect resp.http.i-bar-integer == -11
	expect resp.http.i-bar-saved == "true"
	expect resp.http.i-bar-integer-0 == resp.http.i-bar-integer
	expect resp.http.i-bar-integer-1 == resp.http.i-bar-integer
	expect resp.http.i-bar-saved-0 == "true"
	expect resp.http.i-bar-saved-1 == "true"
	expect resp.http.i-fail-match == "false"
	expect resp.http.i-fail-n == 0
	expect resp.http.i-many-match == "true"
	expect resp.http.i-many-n == 2
	expect resp.http.i-many-first == 47
	expect resp.http.i-many-last == -11
	expect resp.http.i-many-saved-first == "true"
	expect resp.http.i-many-saved-last == "true"
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_b4_match$}
	expect * = VCL_Error {^vmod re2 failure: i\.saved\(\) called without prior match$}
	expect 0 = RespHeader {^i-saved-before-match: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_fail$}
	expect * = VCL_Error {^vmod re2 failure: i\.saved\(0\): previous match was unsuccessful$}
	expect 0 = RespHeader {^i-fail-saved: false$}
	expect 0 = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = ReqHeader {^Test: saved_unique$}
	expect * = VCL_Error {^vmod re2 failure: i\.saved\(0\): 2 successful matches$}
	expect 0 = RespHeader {^i-many-saved: false$}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Test: saved_b4_match"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.i-saved-before-match == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_fail"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.i-fail-saved == <undef>
	expect_close
} -run

client c1 {
	txreq -hdr "Test: saved_unique"
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect resp.http.i-many-saved == <undef>
	expect_close
} -run

logexpect l1 -wait

varnish v1 -vcl {
	import ${vmod_re2};
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new i = re2.set();
		i.add("foo", integer=47);
		i.add("bar", integer=-11);
	}

	sub vcl_recv {
		if (req.url == "/1") {
			set req.http.i-before-match = i.integer();
		}
		if (req.url == "/2") {
			set req.http.i-fail-match = i.match("fail");
			set req.http.i-fail-integer = i.integer();
		}
		if (req.url == "/3") {
			set req.http.i-many-match = i.match("foobar");
			set req.http.i-many-integer = i.integer();
		}
		if (req.url == "/4") {
			set req.http.i-outofrange = i.integer(3);
		}
		return(synth(200));
	}
}

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error {^vmod re2 failure: i\.integer\(\) called without prior match$}
	expect 0 = ReqHeader "i-before-match: 0"
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod re2 failure: i\.integer\(0\): previous match was unsuccessful$}
	expect 0 = ReqHeader "i-fail-integer: 0"
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod re2 failure: i\.integer\(0\): 2 successful matches$}
	expect 0 = ReqHeader "i-many-integer: 0"
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod re2 failure: i\.integer\(3\): set has 2 patterns$}
	expect 0 = ReqHeader "i-outofrange: 0"
	expect * = End
} -start

client c1 {
	txreq -url /1
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect_close
} -run

client c1 {
	txreq -url /2
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect_close
} -run

client c1 {
	txreq -url /3
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect_close
} -run

client c1 {
	txreq -url /4
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect_close
} -run

logexpect l1 -wait
